import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Slides) spotsSlides: Slides;

  slides: any[];
  spots: any[];
  spotsNearYou1: any[];
  spotsNearYou2: any[];
  spotsNearYou3: any[];

  constructor(public navCtrl: NavController) {
    this.slides = [];

    for(let i = 1; i <= 8; i++)
    {
      this.slides.push({
        title: 'Menu ' + i,
        image: 'assets/imgs/food/' + i +'.png'
      });
    }

    this.spotsNearYou1 = [
      "assets/imgs/food/1.png",
      "assets/imgs/food/2.png",
      "assets/imgs/food/3.png"
    ];
    this.spotsNearYou2 = [
      "assets/imgs/food/1.png",
      "assets/imgs/food/2.png"
    ];
    this.spotsNearYou3 = [
      "assets/imgs/food/1.png",
      "assets/imgs/food/2.png",
      "assets/imgs/food/3.png",
      "assets/imgs/food/4.png"
    ];

    this.spots = [
      {
        name: 'Spot 1',
        menu: this.slides,
        spotsnear: this.spotsNearYou1
      },
      {
        name: 'Spot 2',
        menu: this.slides,
        spotsnear: this.spotsNearYou2
      },
      {
        name: 'Spot 3',
        menu: this.slides,
        spotsnear: this.spotsNearYou3
      },
    ];
  
  }

  next() {
    this.spotsSlides.slideNext();
  }

  prev() {
    this.spotsSlides.slidePrev();
  }
}
